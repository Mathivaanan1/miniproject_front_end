import { Component, OnInit, ViewChild } from '@angular/core';
import { ValidationService } from '../validation.service';
import { Stack } from '../stack';
import { Sell } from '../sell';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { SalesComponent } from '../sales/sales.component';

@Component({
  selector: 'app-sell',
  templateUrl: './sell.component.html',
  styleUrls: ['./sell.component.css']
})
export class SellComponent implements OnInit {
 //public sales:any;
 public sales1:any;
  //sell:Stack=new Stack();
  sell:Sell=new Sell();
  public sells:any;
  public displayedColumns:String[]=['id','companyname','count','total','action'];
public dataSource:any;

@ViewChild(MatPaginator) paginator !:MatPaginator;
@ViewChild(MatSort) sort !:MatSort;
  
  constructor(private service:ValidationService ,private dialog:MatDialog){}


  ngOnInit(): void {
   
    this.service.getsell().subscribe((data)=>{
      this.sells=data;
this.dataSource=data;
this.dataSource=new MatTableDataSource<Stack>(this.sells)
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

    });
   }
   filterchange(data:Event){
    const value=(data.target as HTMLInputElement).value;
    this.dataSource.filter=value;  


   }

   popup(id:number){
   

     this.dialog.open(SalesComponent,{width:'40%',height:'47%'});
    
     this.sales1=this.sells[id-1]
     this.service.senddatasales(this.sales1);
    
    }


}
