import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import{ReactiveFormsModule,FormsModule} from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { CompanystockComponent } from './companystock/companystock.component';
import{HttpClientModule} from '@angular/common/http';
import { ChartComponent } from './chart/chart.component';
import { BuyComponent } from './buy/buy.component';
import {MatIconModule} from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import {MatTableModule} from '@angular/material/table';
import{MatPaginatorModule} from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { SellComponent } from './sell/sell.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { SalesComponent } from './sales/sales.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    CompanystockComponent,
    ChartComponent,
    BuyComponent,
    SellComponent,
    SalesComponent
  ],
  imports: [
    BrowserModule,MatCardModule,MatToolbarModule,
    AppRoutingModule,ReactiveFormsModule,FormsModule,MatTableModule,MatPaginatorModule,
    BrowserAnimationsModule,HttpClientModule,MatIconModule,MatDialogModule, MatSortModule,
    FlexLayoutModule,MatDatepickerModule,MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
