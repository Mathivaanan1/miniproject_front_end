import { Component, OnInit } from '@angular/core';
import { ValidationService } from '../validation.service';
import{FormControl,FormGroup,Validators} from '@angular/forms'
import { Adminlog } from '../adminlog';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  private baseurl="http://localhost:8080";
 logindata:Adminlog=new Adminlog();
  name: any;
  
constructor(public http:HttpClient,
  public router:Router,public service:ValidationService){}


  ngOnInit(): void {
   
   }

   userforms:FormGroup=new FormGroup({
  username:new FormControl('',[Validators.required,Validators.minLength(4),Validators.maxLength(6)]),
    password:new FormControl('',[Validators.required,Validators.minLength(4),Validators.maxLength(6)])
   });


   

  submitdata(){
    this.http.post(this.baseurl+"/company/admin",this.logindata).subscribe(data=>{
 //
      this.next();
      alert("login success");
    },
    
   (error)=>{
   alert("login and password invalid");
   }
    );
  }
  
  next(){
    this.router.navigateByUrl('/companystock');


  }

// amountvalue(event:any){
 
//  console.log(event);
//  // this.service.getamount(name).subscribe((data)=>{
// //console.log(data);
//  // })
// }

setdata(event:any){
//this.service.setdata(event);
let name=event;
// this.service.getamount(name).subscribe((data)=>{
//  console.log(data);
// })

//checking after delete
 this.service.sentdata(name);
}

  }
  
 



