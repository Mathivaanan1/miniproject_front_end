import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ValidationService } from '../validation.service';
import { Stack } from '../stack';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  constructor(public service:ValidationService){}

  public chart:any;
// data:any[] | undefined;
stack:Stack[]=[];
stack1:Stack[]=[];
data:any;

// graphData: Stack[]=[];
// companyName: String[]=[''];
// price: String[]=[];


  ngOnInit(){
   this.service.getchart().subscribe((data)=>{
      this.stack=data;

    });










// this.chart=new Chart('mychart',{

// type:'bar',
// data:{
//   labels:['monday','tuesday','wednesday','thursday','friday'],

//   datasets: [{
//     label:'This week Stack Price',
//     data:[700,500,300,200,500,0],
//     backgroundColor:[
//       'rgba(255, 99, 132, 0.2)',
//       'rgba(255, 159, 64, 0.2)',
//       'rgba(255, 205, 86, 0.2)',
//       'rgba(75, 192, 192, 0.2)',
//       'rgba(54, 162, 235, 0.2)',
//       'rgba(153, 102, 255, 0.2)',
//       'rgba(201, 203, 207, 0.2)'
//     ],
//     fill:true,
//   }],
// },

// });

  

  }

  getcompany(cname:any){
  let name=cname;
  this.service.getone(name).subscribe((data)=>{
this.stack1=data;
console.log(data);
this.createchart();

  });
  }

  createchart(){
      
    let labelName=['MONDAY','TUESDAY','WEDNESDAY','THURSDAY','FRIDAY'];
    let dataSize=[];
    for(let i=0;i<this.stack1.length;i++){
      //labelName.push(this.stack[i].companyname);
      dataSize.push(this.stack1[i].price);
    }
    this.chart=new Chart('mychart',{
          type:'line',
          data:{
            labels:labelName,
            datasets:[
              {
                label:'this week stock price',
                data:dataSize,
                borderWidth:1,

                backgroundColor:[
                         'rgba(106, 90, 205)',
                    
                      ],


                fill:true,
              },
            ],
          },
          options:{
            aspectRatio:5.5,
          },

    });

  }
  

}
