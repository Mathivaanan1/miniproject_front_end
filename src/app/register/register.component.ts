import { Component, OnInit } from '@angular/core';
import { ValidationService } from '../validation.service';
import{FormControl,FormGroup,Validators} from '@angular/forms'
import { Model } from '../model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  //email: any;
 
  registerdata:Model=new Model();
  
 constructor(public urlservice:ValidationService,
  public router:Router){}
 
  ngOnInit(): void {
    
  }
submitdata(){
  this.urlservice .createregister(this.registerdata).subscribe(data=>{
    
    this.next();
    alert("Registration Success");
  });
}
next(){
  this.router.navigateByUrl('/login');
}










  userforms:FormGroup=new FormGroup({
     //id:new FormControl('',Validators.required),
    username:new FormControl('',[Validators.required,Validators.minLength(4),Validators.maxLength(6)]),
    password:new FormControl('',[Validators.required,Validators.minLength(4),Validators.maxLength(6)]),
    dob:new FormControl('',[Validators.required]),
    email:new FormControl('',[Validators.required,Validators.email]),
    pan:new FormControl('',Validators.required),
    totalamount:new FormControl('',[Validators.required,Validators.min(1000)])
    
  
  
   });

 }
