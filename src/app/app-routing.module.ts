import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { CompanystockComponent } from './companystock/companystock.component';
import { ChartComponent } from './chart/chart.component';
import { BuyComponent } from './buy/buy.component';
import { SellComponent } from './sell/sell.component';
import { SalesComponent } from './sales/sales.component';

const routes: Routes = [
  {path:'',component:LoginComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'companystock',component:CompanystockComponent},
  {path:'chart',component:ChartComponent},
  {path:'buy',component:BuyComponent},
  {path:'sell',component:SellComponent},
  {path:'sales',component:SalesComponent}
 // {path:'buy/:id',component:BuyComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
