import { Component } from '@angular/core';
import { ValidationService } from '../validation.service';
import { Stack } from '../stack';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Sell } from '../sell';
import { Model } from '../model';
//import { SalesComponent } from '../sales/sales.component';

@Component({
  selector: 'app-buy',
  templateUrl: './buy.component.html',
  styleUrls: ['./buy.component.css']
})
export class BuyComponent {
  selldatas:Sell=new Sell();
  //stacks:Stack=new Stack();

  buyproduct:Stack=new Stack();
  amount:Model=new Model();

  counts:Number=0;

  totals:any;
total:any;
  constructor(public service:ValidationService,public remove:MatDialogRef<BuyComponent>,public router:Router,private dialog:MatDialog){}
  ngOnInit(): void {
    this.buyproduct=this.service.receive();

    
  }

visible:boolean=false;

pricevalue(event:any){
if(event===this.counts){

}

let con=Number(event.data)
console.log(con);

this.visible=!this.visible;
let rate=Number(this.buyproduct.price)

//console.log(this.counts);
this.total = (con * rate);
console.log(this.total);

}
sell(){
 this.next();
  alert("stock purchased");
  this.close();
}
close(){
  this.remove.close();
}
next(){
this.router.navigateByUrl('/sell');
}

submitdata(){
  this.selldatas.total=this.total
  this.selldatas.companyname=this.buyproduct.companyname;
this.service.sellsave(this.selldatas).subscribe((data)=>{
   

});
}
amountdata(){
this.service.getamount().subscribe((data)=>{
  this.amount=data;
  this.totals=this.amount.toString();
  
  //console.log(this.totals);

  if(this.totals>=this.total){
    this.sell();
    this.submitdata();
  }else{
    alert("your balance low ="+this.totals)
  }

})


}




}