//import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ValidationService } from '../validation.service';
//import { Stack } from '../stack';
import { BuyComponent } from '../buy/buy.component';
import{MatDialog} from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Stack } from '../stack';
import { Router } from '@angular/router';
@Component({
  selector: 'app-companystock',
  templateUrl: './companystock.component.html',
  styleUrls: ['./companystock.component.css']
})
export class CompanystockComponent implements OnInit {
public stacks:any;  
stacks1:any;
public displayedColumns:String[]=['id','companyname','date','price','action'];
public dataSource:any;
@ViewChild(MatPaginator) paginator !:MatPaginator;
@ViewChild(MatSort) sort !:MatSort;
  
  constructor(public service:ValidationService,private dialog:MatDialog,public router: Router){}
  ngOnInit(): void {
    
    this.service.getuser().subscribe((data)=>{
      this.stacks=data;
      //this.dataSource=data;
      this.dataSource=new MatTableDataSource<Stack>(this.stacks)
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  

  }

  popup(id:number){
   

      this.dialog.open(BuyComponent,{width:'30%',height:'47%'});
      
      this.stacks1=this.stacks[id-1]
      this.service.senddata(this.stacks1);
      
      }
      
      filterchange(data:Event){
         const value=(data.target as HTMLInputElement).value;
          this.dataSource.filter=value; 
         // console.log(this.dataSource.filter=value) 

      }
      graph(){
        
         //this.service.sendGraphData(this.dataSource.filter)
         this.chart();
      }
      chart(){
        this.router.navigateByUrl("/chart")
      }

      // get(id:number){
      //   this.stacks1=this.stacks[id-1]
      //   this.service.send(this.stacks1);
      // }

  }
 


