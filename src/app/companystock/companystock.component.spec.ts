import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanystockComponent } from './companystock.component';

describe('CompanystockComponent', () => {
  let component: CompanystockComponent;
  let fixture: ComponentFixture<CompanystockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanystockComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CompanystockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
