import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Model } from './model';
import { Observable } from 'rxjs';
import { Stack } from './stack';
import { Sell } from './sell';
//import { validateBasis } from '@angular/flex-layout';
//import{FormControl,FormGroup,Validators} from '@angular/forms'
//import { min } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ValidationService implements OnInit{
  private total:any;
 buyproduct:any;
 salesproduct:any;
 //companystock data recived here for popup send to buycomponent
 senddata(pro:any){
 this.buyproduct=pro;
 }

 receive(){
  return this.buyproduct;
 }
 
 private baseurl="http://localhost:8080";

 private url="http://localhost:8080/stack/chart";
 private sellurl="http://localhost:8080";
private amurl="http://localhost:8080/company/amount"
  constructor(public http:HttpClient) { }
  ngOnInit(): void {
    
  }
createregister(register:Model):Observable<object>{

  return this.http.post(this.baseurl+"/company/registration",register);
}
 
getuser(){
  return this.http.get<Stack[]>("http://localhost:8080/stack/get");
}

sellsave(selldata:Sell):Observable<object>{
return this.http.post(this.sellurl+"/stack/post",selldata)
}


getone(companyname:any){
  return this.http.get<Stack[]>(`${this.url}/${companyname}`);
}
getsell(){
  return this.http.get("http://localhost:8080/stack/sell")
}

getchart(){
  return this.http.get<Stack[]>("http://localhost:8080/company/chart");
}

getamount(){
  //console.log(this.total);
  return this.http.get<Model>(`${this.amurl}/${this.total}`);
}



// getcompanies(name:any){

//   let companyby=name;
//   console.log(companyby);
//   return this.http.get<Stack[]>(
//     ''
//   )
// }

//it is receive data from login page
sentdata(username:any){
  this.total=username;
  //console.log(this.total);
}


//this is get data from sellcomponent and send  popup to salaes component

senddatasales(pro:any){
this.salesproduct=pro;
}
receivesales(){
  return this.salesproduct;
 }

}
