import { Component, OnInit } from '@angular/core';
import { ValidationService } from '../validation.service';
import { Sell } from '../sell';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css']
})
export class SalesComponent implements OnInit {
saleproduct:Sell=new Sell();
  constructor(public service:ValidationService,public remove:MatDialogRef<SalesComponent>){}

  ngOnInit(): void {
    this.saleproduct=this.service.receivesales();
  }

  sales(){
    alert("stcok selled successfully");
this.close();
  }
  close(){
    this.remove.close();
  }

}
